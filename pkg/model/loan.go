package model

type Loan struct {
	Id              string `json:"id"`
	Due             string `json:"due"`
	RepaymentAmount int    `json:"repaymentAmount"`
	Status          string `json:"status"`
	Type            string `json:"type"`
}
