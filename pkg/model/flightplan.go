package model

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"go-spacetrade/pkg/api"
)

type FlightPlanResponse struct {
	FlightPlan FlightPlan `json:"flightPlan"`
}

type FlightPlan struct {
	ShipId      string `json:"shipId"`
	Id          string `json:"id"`
	Destination string `json:"destination"`
}

func (plan FlightPlan) Submit(user User) error {
	log.Log().Msgf("submitting flight plan - %+v", plan)
	var planR FlightPlanResponse
	url := fmt.Sprintf("https://api.spacetraders.io/users/%s/flight-plans?token=%s&shipId=%s", user.Username, user.Token, plan.ShipId)
	err := api.Post(url, plan, &planR)
	if err != nil {
		return err
	}

	plan.Id = planR.FlightPlan.Id

	return nil
}
