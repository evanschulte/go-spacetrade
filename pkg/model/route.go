package model

import (
	"errors"
	"github.com/rs/zerolog/log"
	"time"
)

type Route struct {
	Points []RoutePoint
}

func (rp *Route) Do(user *User, ship *Ship) error {
	var waitFor int64 = 0
	var waitStart *int64
	var err error
	for err == nil {
		ws := time.Now().UnixNano()
		if waitStart == nil || ws > (*waitStart+waitFor) {
			foundPoint := false
			for _, point := range rp.Points {
				if point.Location == ship.Location {
					foundPoint = true
					for _, rOrder := range point.Orders {
						if rOrder.Type == "SELL" {
							_, _ = user.SellOrder(*ship, rOrder.Order)
						} else if rOrder.Type == "BUY" {
							_, err = user.PurchaseOrder(*ship, rOrder.Order)
						}
					}
					_, err = user.NewFlightPlan(*ship, point.TravelTo)
					waitStart = &ws
					waitFor = point.TravelTime + int64(time.Second*5) // 5 second buffer
					ship.Location = point.TravelTo
					log.Log().Msgf("waiting for %d milliseconds while traveling to %s", waitFor/int64(time.Millisecond), point.TravelTo)
					break
				}
			}
			if !foundPoint {
				return errors.New("no route point found for current ship location")
			}
		}
		time.Sleep(time.Second * 1)
	}
	return err
}

type RoutePoint struct {
	Location   string
	TravelTo   string
	TravelTime int64
	Orders     []RouteOrder
}

type RouteOrder struct {
	Type  string
	Order Order
}
