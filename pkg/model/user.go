package model

import (
	"github.com/rs/zerolog/log"
	"go-spacetrade/pkg/api"
)

type UserResponse struct {
	User User `json:"user"`
}

type User struct {
	Username string `json:"username"`
	Token    string `json:"token"`
	Credits  int64  `json:"credits"`
	Ships    []Ship `json:"ships"`
	Loans    []Loan `json:"loans"`
}

func (user *User) Fetch() error {
	var r UserResponse
	log.Log().Msgf("fetching user - %+v", user)
	err := api.Get("https://api.spacetraders.io/users/"+user.Username+"?token="+user.Token, &r)
	if err != nil {
		return err
	}

	user.Username = r.User.Username
	user.Credits = r.User.Credits
	user.Ships = r.User.Ships
	user.Loans = r.User.Loans

	return nil
}

func (user *User) NewPurchaseOrder(ship Ship, good string, quantity int) (*Order, error) {
	order := Order{
		Good:     good,
		Quantity: quantity,
	}
	return user.PurchaseOrder(ship, order)
}

func (user *User) PurchaseOrder(ship Ship, order Order) (*Order, error) {
	err := order.Purchase(*user)
	if err != nil {
		return nil, err
	}
	return &order, nil
}

func (user *User) NewSellOrder(ship Ship, good string, quantity int) (*Order, error) {
	order := Order{
		Good:     good,
		Quantity: quantity,
	}
	return user.SellOrder(ship, order)
}

func (user *User) SellOrder(ship Ship, order Order) (*Order, error) {
	err := order.Sell(*user)
	if err != nil {
		return nil, err
	}
	return &order, nil
}

func (user *User) NewFlightPlan(ship Ship, destination string) (*FlightPlan, error) {
	plan := FlightPlan{
		ShipId:      ship.Id,
		Destination: destination,
	}
	err := plan.Submit(*user)
	if err != nil {
		return nil, err
	}
	return &plan, nil
}
