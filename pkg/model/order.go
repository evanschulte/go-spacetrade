package model

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"go-spacetrade/pkg/api"
)

type OrderResponse struct {
	Order []Order `json:"order"`
	Ship  Ship    `json:"ship"`
}

type Order struct {
	ShipId       string `json:"shipId"`
	Good         string `json:"good"`
	Quantity     int    `json:"quantity"`
	PricePerUnit int    `json:"pricePerUnit"`
	Total        int    `json:"total"`
}

func (order Order) Purchase(user User) error {
	log.Log().Msgf("sending purchase order - %+v", order)
	var por OrderResponse
	err := api.Post(fmt.Sprintf("https://api.spacetraders.io/users/%s/purchase-orders?token=%s&shipId=%s", user.Username, user.Token, order.ShipId), &order, &por)
	if err != nil {
		return err
	}

	return nil
}

func (order Order) Sell(user User) error {
	log.Log().Msgf("sending sell order - %+v", order)
	var por OrderResponse
	err := api.Post(fmt.Sprintf("https://api.spacetraders.io/users/%s/sell-orders?token=%s&shipId=%s", user.Username, user.Token, order.ShipId), &order, &por)
	if err != nil {
		return err
	}

	return nil
}
