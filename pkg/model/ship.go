package model

type Ship struct {
	Id             string    `json:"Id"`
	Token          string    `json:"token"`
	Location       string    `json:"location"`
	X              int       `json:"x"`
	Y              int       `json:"y"`
	Cargo          []Product `json:"cargo"`
	MaxCargo       int       `json:"maxCargo"`
	SpaceAvailable int       `json:"spaceAvailable"`
	Type           string    `json:"type"`
	Class          string    `json:"class"`
	Speed          int       `json:"speed"`
	Manufacturer   string    `json:"manufacturer"`
	Plating        int       `json:"plating"`
	Weapons        int       `json:"weapons"`
	FlightPlanId   *string   `json:"flightPlanId"`
}
