package model

type Product struct {
	Symbol        string `json:"symbol"`
	VolumePerUnit int    `json:"volumePerUnit"`
	PricePerUnit  int    `json:"pricePerUnit"`
}
