package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"net/http"
	"time"
)

var client = &http.Client{Timeout: 10 * time.Second}
var rateLimitDelay time.Duration = 1200
var maxRetries = 2

func Get(url string, target interface{}) error {
	return get(url, target, 0)
}

func get(url string, target interface{}, retryCount int) error {
	res, _ := client.Get(url)
	defer res.Body.Close()
	retry, err := handleResponse(res)
	time.Sleep(rateLimitDelay)
	if retry && retryCount < maxRetries {
		return get(url, target, retryCount)
	} else if err != nil {
		return err
	}

	err = json.NewDecoder(res.Body).Decode(target)

	return err
}

func Post(url string, body interface{}, target interface{}) error {
	return post(url, body, target, 0)
}

func post(url string, body interface{}, target interface{}, retryCount int) error {
	jsonBody, _ := json.Marshal(body)

	res, _ := client.Post(url, "application/json", bytes.NewBuffer(jsonBody))
	defer res.Body.Close()
	retry, err := handleResponse(res)
	time.Sleep(rateLimitDelay)
	if retry && retryCount < maxRetries {
		return post(url, body, target, retryCount)
	} else if err != nil {
		return err
	}

	err = json.NewDecoder(res.Body).Decode(target)

	return err
}

func handleResponse(res *http.Response) (bool, error) {
	retry := false
	var err error
	switch res.StatusCode {
	case 429:
		retry = true
		break
	case 400:
		type APIError struct {
			Message string `json:"message"`
			Code    int    `json:"code"`
		}
		type APIErrorResponse struct {
			Error APIError `json:"error"`
		}
		var aErr APIErrorResponse
		err = json.NewDecoder(res.Body).Decode(&aErr)
		log.Error().Msgf("400 error - %+v\n", aErr)
		break
	case 200:
	case 201:
		err = nil
	default:
		err = errors.New(fmt.Sprintf("unrecognized status code: %d", res.StatusCode))
		break
	}
	return retry, err
}
