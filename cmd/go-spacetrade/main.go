package main

import (
	_ "github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
	"go-spacetrade/pkg/logging"
	"go-spacetrade/pkg/model"
	_ "net/http/pprof"
	"os"
	"sync"
	"time"
)

func main() {
	app := cli.NewApp()
	app.Name = "go-spacetrace"
	app.Usage = "a bot for spacetrade.io"
	app.Flags = []cli.Flag{
		&cli.BoolFlag{Name: "debug"},
	}
	app.Authors = []*cli.Author{
		{
			Name:  "Evan Schulte",
			Email: "evan.schulte@gmail.com",
		},
	}
	app.Copyright = "(c) 2020 Evan Schulte"
	app.Version = "1.0.0"
	app.Compiled = time.Now()
	app.Before = func(c *cli.Context) error {
		logging.Init()
		return nil
	}
	app.Commands = []*cli.Command{
		{
			Name:  "start",
			Usage: "",
			Flags: []cli.Flag{
				&cli.StringFlag{Name: "username", Aliases: []string{"u"}},
				&cli.StringFlag{Name: "token", Aliases: []string{"t"}},
			},
			Action: func(c *cli.Context) error {
				username := c.String("username")
				token := c.String("token")
				user := model.User{
					Username: username,
					Token:    token,
				}

				err := user.Fetch()
				if err != nil {
					log.Error().Msgf("failed to get user: %+v - %+v\n", user, err)
					return err
				}

				var wg sync.WaitGroup
				for _, ship := range user.Ships {
					if len(ship.Location) > 0 {
						wg.Add(1)
						go moneyMaker(&wg, user, ship)

						// wait a few seconds to try and avoid rate limits
						time.Sleep(time.Second * 3)
					}
				}
				wg.Wait()
				return err
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal().Err(err)
	}
}

func moneyMaker(wg *sync.WaitGroup, user model.User, ship model.Ship) {
	defer wg.Done()

	shipPartsOrder := model.Order{
		ShipId:   ship.Id,
		Good:     "SHIP_PARTS",
		Quantity: 19,
	}
	metalsOrder := model.Order{
		ShipId:   ship.Id,
		Good:     "METALS",
		Quantity: 95,
	}
	metalsBuy := model.RouteOrder{Order: metalsOrder, Type: "BUY"}
	metalsSell := model.RouteOrder{Order: metalsOrder, Type: "SELL"}

	shipPartsBuy := model.RouteOrder{Order: shipPartsOrder, Type: "BUY"}
	shipPartsSell := model.RouteOrder{Order: shipPartsOrder, Type: "SELL"}

	route := model.Route{Points: []model.RoutePoint{
		{
			Location: "OE-PM",
			Orders: []model.RouteOrder{
				metalsSell,
				{Order: model.Order{
					ShipId:   ship.Id,
					Good:     "FUEL",
					Quantity: 4,
				}, Type: "BUY"},
				shipPartsBuy,
			},
			TravelTo:   "OE-PM-TR",
			TravelTime: int64(time.Second * 90),
		},
		{
			Location: "OE-PM-TR",
			Orders: []model.RouteOrder{
				shipPartsSell,
				{Order: model.Order{
					ShipId:   ship.Id,
					Good:     "FUEL",
					Quantity: 2,
				}, Type: "BUY"},
				metalsBuy,
			},
			TravelTo:   "OE-PM",
			TravelTime: int64(time.Second * 90),
		},
	}}

	err := route.Do(&user, &ship)
	if err != nil {
		log.Error().Msgf("failed to do route - %+v", err)
	}
}
