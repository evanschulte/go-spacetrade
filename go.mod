module go-spacetrade

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	github.com/rs/zerolog v1.20.0
	github.com/urfave/cli/v2 v2.2.0
)
